from __future__ import print_function
import sys
import csv
import os.path

__version__ = '1.2'
__author__ = 'Nick Heaphy'
__home_url__ = "https://bitbucket.org/nickheaphy/python-vdp-preprocessors"

# ------------------------------
def validate_csv_return_rows(csv_to_test):
    '''This does a test open of the CSV and counts the rows'''
    print('Validating CSV...')
    try:
        with open(csv_to_test, 'r', encoding='utf-8-sig') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            row_counter = 0
            for row in reader:
                row_counter += 1
        print("Found %s rows" % str(row_counter))
    except:
        print("Sorry, there was a problem with this CSV.")
        print(sys.exc_info()[0])
        exit(1)
    return row_counter

# ------------------------------
def main(csv_to_open):

    __csvfolder__, __csvfile__ = os.path.split(os.path.abspath(sys.argv[1]))

    row_counter_original = validate_csv_return_rows(csv_to_open)

    with open(csv_to_open, 'r', encoding='utf-8-sig') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        headerrow = next(reader)
        print("Found %s column headers:" % len(headerrow))
        for i, key in enumerate(headerrow):
            print("%s : %s" % (i, key))

        print("Please enter columns to combine...")
        r1 = input('First Column: ')
        r2 = input('Second Column: ')
        sep = input('Enter the seperator (ie press space then enter for a space)')

        newfilename = "join_%s" % __csvfile__
        with open(os.path.join(__csvfolder__,newfilename), 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            newcolumn = ['JOIN_%s_%s' % (headerrow[int(r1)],headerrow[int(r2)])]
            newcolumn.extend(headerrow)
            writer.writerow(newcolumn)
            row_counter = 0
            for row in reader:
                extract1 = row[int(r1)].strip()
                extract2 = row[int(r2)].strip()
                if extract1 != "" and extract2 != "":
                    newrowdata = "%s%s%s" % (extract1, sep, extract2)
                elif extract1 != "" and extract2 == "":
                    newrowdata = extract1
                else:
                    newrowdata = extract2
                row.insert(0,newrowdata)

                writer.writerow(row)
                row_counter += 1
            
        print("Processed %s rows" % str(row_counter+1))
        if row_counter_original != row_counter+1:
            print("Something bad might have happened. I seem to have processed a different number of rows")

        
# ------------------------------
if __name__ == "__main__":
    # Check the passed parameters
    if len(sys.argv) != 2:
        print("Need to pass the csv file on the commandline")
        exit()
    
    if sys.version_info[0] != 3:
        print("Sorry, needs Python 3 to handle Unicode CSV files")
        exit(1)

    # Check the passed parameters
    if len(sys.argv) != 2:
        print("Need to pass the csv file on the commandline")
        exit(1)
    
    if not os.path.isfile(sys.argv[1]):
        print("Can't open the CSV file")
        exit(1)

    main(sys.argv[1])
    
        