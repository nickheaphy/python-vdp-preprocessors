from __future__ import print_function
import sys
import csv
import os.path

__version__ = '1.2'
__author__ = 'Nick Heaphy'
__home_url__ = "https://bitbucket.org/nickheaphy/python-vdp-preprocessors"

# ------------------------------
def validate_csv_return_rows(csv_to_test):
    '''This does a test open of the CSV and counts the rows'''
    print('Validating CSV...')
    try:
        with open(csv_to_test, 'r', encoding='utf-8-sig') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            row_counter = 0
            for row in reader:
                row_counter += 1
        print("Found %s rows" % str(row_counter))
    except:
        print("Sorry, there was a problem with this CSV.")
        print(sys.exc_info()[0])
        exit(1)
    return row_counter

# ------------------------------
def pad_or_truncate(some_list, target_len):
    return some_list[:target_len] + ['']*(target_len - len(some_list))

# ------------------------------
def main(csv_to_open):

    __csvfolder__, __csvfile__ = os.path.split(os.path.abspath(sys.argv[1]))

    row_counter_original = validate_csv_return_rows(csv_to_open)

    with open(csv_to_open, 'r', encoding='utf-8-sig') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        headerrow = next(reader)
        print("Found %s column headers:" % len(headerrow))
        for i, key in enumerate(headerrow):
            print("%s : %s" % (i, key))

        print("Please enter columns to collapse between...")
        c1 = input('Column Start: ')
        c2 = input('Column End: ')

        print("Collapsing columns between: %s and %s" % (c1, c2))
        print("Do you want to join a column to the last piece of populated data (eg a postcode column?")
        joinc = input("Enter column number (otherwise enter to bypass) ")

        newfilename = "collapse_%s" % __csvfile__
        with open(os.path.join(__csvfolder__,newfilename), 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            newheader = []
            for i in range(int(c1), int(c2)+1):
                newheader.append("COLLAPSE_%s" % headerrow[i])
            newheader.extend(headerrow)
            writer.writerow(newheader)
            row_counter = 0
            for row in reader:
                # Extract the bit to collapse
                subrow = row[int(c1):int(c2)+1]
                clean_subrow = list(filter(None, subrow))
                if joinc != "":
                    clean_subrow[-1] = "%s %s" % (clean_subrow[-1], row[int(joinc)])
                clean_subrow = pad_or_truncate(clean_subrow,len(subrow))
                clean_subrow.extend(row)
                #print(clean_subrow)
                #print(row)
                writer.writerow(clean_subrow)
                row_counter += 1
        
        print("Processed %s rows" % str(row_counter+1))
        if row_counter_original != row_counter+1:
            print("Something bad might have happened. I seem to have processed a different number of rows")

        
# ------------------------------
if __name__ == "__main__":
    # Check the passed parameters
    if len(sys.argv) != 2:
        print("Need to pass the csv file on the commandline")
        exit()
    
    if sys.version_info[0] != 3:
        print("Sorry, needs Python 3 to handle Unicode CSV files")
        exit(1)

    # Check the passed parameters
    if len(sys.argv) != 2:
        print("Need to pass the csv file on the commandline")
        exit(1)
    
    if not os.path.isfile(sys.argv[1]):
        print("Can't open the CSV file")
        exit(1)

    main(sys.argv[1])