# VDP Preprocessor Scripts

Some Python 3 scripts to perform specific cleanup functions to spreadsheets to make variable data printing possible.

-----------------------

-----------------------

### joinColumns.py

joinColumns allows you to specify two columns to join together and the character(s) that you want to use between the columns. In the advent that either of the columns is blank, then the join character(s) are omitted.

Using the following data:

| Firstname     | Surname       | 
| ------------- | ------------- |
| Fred          | Smith         |
|               | Big Company   |

If we joined Firstname and Surname using a " - " as the separator we would get the following (note that the second row does not include the separator!)

| JOIN_Firstname_Surname | Firstname     | Surname       | 
| ---------------------- | ------------  | ------------- |
| Fred - Smith           | Fred          | Smith         |
| Big Company            |               | Big Company   |      

-----------------------

-----------------------

### collapseColumns.py

collapseColumns allow you to reorder a range of columns to eliminate missing data. Optionally you can combine a column with the last piece of populated data. This allows you to include postcode data.

This allows you to process the following badly formed data

| Address1        | Address2         | Address3    | Address4     | Postcode |
| --------------- | ---------------- | ----------- | ------------ | -------- |
| 1 Hunter Street | Wellington       |             |              | 6011     |
| 24 Norton Road  | Frankton         |             | Hamilton     | 3204     |
| Akoranga Park   | 28 Warehouse Way | Northcote   | Auckland     | 0627     |

And will create

| COLLAPSE_Address1 | COLLAPSE_Address2 | COLLAPSE_Address3 | COLLAPSE_Address4 | 
| ----------------- | ----------------- | ----------------- | ----------------- | 
| 1 Hunter Street   | Wellington 6011   |                   |                   | 
| 24 Norton Road    | Frankton          | Hamilton 3204     |                   | 
| Akoranga Park     | 28 Warehouse Way  | Northcote         | Auckland 0627     |

Now in your VPD package, after enabling _skip blank lines_ you can use the following

```
    <<COLLAPSE_Address1>>
    <<COLLAPSE_Address2>>
    <<COLLAPSE_Address3>>
    <<COLLAPSE_Address4>>
```

And you will get the correct addresses.

-----------------------

-----------------------

### justifyColumns.py

This is a variation on the collapseColumns.py that is probably a better way to handle postcodes.

It takes the data like this

| Address1        | Address2         | Address3    | Address4     | Postcode |
| --------------- | ---------------- | ----------- | ------------ | -------- |
| 1 Hunter Street | Wellington       |             |              | 6011     |
| 24 Norton Road  | Frankton         |             | Hamilton     | 3204     |
| Akoranga Park   | 28 Warehouse Way | Northcote   | Auckland     | 0627     |

and will create

| JUSTIFY_Address1 | JUSTIFY_Address2 | JUSTIFY_Address3 | JUSTIFY_Address4  | Postcode |
| ---------------- | ---------------- | ---------------- | ----------------- | -------- |
| 1 Hunter Street  |                  |                  | Wellington        | 6011     |
| 24 Norton Road   | Frankton         |                  | Hamilton          | 3204     |
| Akoranga Park    | 28 Warehouse Way | Northcote        | Auckland          | 0627     |

Now in your VPD package, after enabling _skip blank lines_ you can use the following

```
    <<JUSTIFY_Address1>>
    <<JUSTIFY_Address2>>
    <<JUSTIFY_Address3>>
    <<JUSTIFY_Address4>> <<Postcode>>
```

Now that JUSTIFY_Address4 always contains the city data, you will get the correct addresses. 

-----------------------

-----------------------

### padColumns.py

padColumns does a simple padding to the columns. You can choose and length and a padding character.

Using the following data:

| Postcode      |
| ------------- |
| 123           |
| 5679          |

Choosing a length of 4 and a padding character of 0 would produce

| PAD_Postcode  |
| ------------- |
| 0123          |
| 5679          |

**Note be careful of Excels tendency to remove leading 0's off columns it thinks are numbers! You can't (easily) use Excel to check the resulting CSV file as opening it will strip the leading zeros!**

If you are using OSX you can use Quickview (press space after selecting the file in Finder) to check the padding.

-----------------------

-----------------------

### Note

All scripts create a new CSV file and create additional columns in the new CSV to allow you to check the new columns against the old columns to ensure the new data is correct.

-----------------------

## Installation

The scripts do require Python 3 as Python 2 does not handle UTF encoded CSVs very well (and if just using regular CSV you can have issues with extended character sets)

### Mac OSX

On OSX Apple only ship Python 2.7 so you need to install Python 3. This is reasonably straigtfowrd using `HomeBrew` https://brew.sh/

First install Brew by entering the following in Terminal `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

Then once installed enter the following in Terminal `brew install python3`

A video detailing the steps is here: http://youtu.be/JqwZXqH7i1w

Download the [repository](https://bitbucket.org/nickheaphy/python-vdp-preprocessors/get/master.zip) to a folder (or use GIT to clone the repository `git clone https://nickheaphy@bitbucket.org/nickheaphy/python-vdp-preprocessors.git`)

### Windows

Download and install the latest Python 3 from https://www.python.org/downloads/windows/

Download the [repository](https://bitbucket.org/nickheaphy/python-vdp-preprocessors/get/master.zip) to a folder.

## Using the scripts

First save your Excel data as a UTF-8 CSV file:

![Saving from Excel](./screenshots/save_excel.png)

### joinColumns.py

Within the folder you extracted the repository run the command `python3 joinColumns.py sample_data.csv` (where sample_data.csv is the path to the data file you want to process)

You will be prompted to enter the column number of the two columns you want to join together and the character you want to use as a separator (if this should be a space, then press the spacebar followed by the enter/return key)

A new CSV will be written called `JOIN_<originalname>.csv`

![Join Example](./screenshots/join_example.png)

### collapseColumns.py

Within the folder you extracted the repository run the command `python3 collapseColumns.py sample_data.csv` (where sample_data.csv is the path to the data file you want to process)

You will be prompted to enter the first column and the last column to collapse between. Optionally you can include another column into the collapse process. The data from this column will be joined (using a space) with the last populated column in the selected range.

A new CSV will be written called `COLLAPSE_<originalname>.csv`

![Collapse Example](./screenshots/collapse_example.png)

### justifyColumns.py

Within the folder you extracted the repository run the command `python3 justifyColumns.py sample_data.csv` (where sample_data.csv is the path to the data file you want to process)

You will be prompted to enter the first column and the last column to justify between (you may need to reorder the columns in Excel before doing this). 

A new CSV will be written called `JUSTIFY_<originalname>.csv`

### padColumns.py

Within the folder you extracted the repository run the command `python3 padolumns.py sample_data.csv` (where sample_data.csv is the path to the data file you want to process)

You will be prompted to enter the column to pad, how many characters long the field should be and the character you want to use to pad with.

A new CSV will be written called `PAD_<originalname>.csv`

### Note

You can combine the scripts together, by feeding the output of the first script to the second script:

>     python3 joinColumns.py sample.csv
>
>     python3 justifyColumns.py join_sample.csv
>
>     python3 padColumns.py justify_join_sample.csv

## Video

There is a video showing processing the mock sample file that is included in the repository that can be viewed here: http://youtu.be/9KDXXevBG-s

# Warning

Use at own risk. Only limited testing has been done! Please carefully check resulting data for any problems.